const layout1 = `<table style="color:#FFFFFF;background: #ffffff" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td valign="top" width="120">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td id="photo">
    <table cellspacing="0" cellpadding="0" id="profilePhoto">
        <tr>
            <td>
            <img src="https://via.placeholder.com/150?text=Profile+Photo" style="max-width:128px;display:block" width="130">
            </td>
        <tr>
            <td height="15">
            </td>
        </tr>
    </table>
</td>
</tr>
<tr align="center">
<td valign="top" id="logourl">
<img src="https://via.placeholder.com/145x50?text=Logo" style="max-width:130px;display:block" width="130">
</td>
</tr>
<tr>
<td height="20"></td>
</tr>
<tr>
<td align="center">
<table cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td valign="top"  id="facebook" ><a href="https://www.facebook.com/hicaliberdevelopers" color="#F36E21" style="display:inline-block;padding:0px;background-color:#f36e21" target="_blank">
<img src="https://www.hicaliber.com.au/wp-content/uploads/2019/09/facebook-icon-2x.png" alt="facebook" color="#F36E21" style="max-width:135px;display:block" height="24"></a></td>
<td valign="top" id="twitter"></td>
<td valign="top" id="linkedin"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</table>
</td>
<td width="30">&nbsp;</td>
<td valign="top">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td>
<h3 color="#000000" style="margin:0px;font-size:18px;color:#000000;font-weight:700;" ><span class="fs" style="" id="name">Your Name<span></h3>
<p color="#000000" style="margin:0px;color:#000000;font-size:14px;line-height:22px"><span class="fs" style="" id="jobtitle">Your Job Title</span></p>
<p color="#000000" style="margin:0px;color:#000000;font-size:14px;line-height:22px"><span class="fs" style="" id="companyName">Hicaliber</span></p></td>
</tr>
<tr>
<td height="18">&nbsp;</td>
</tr>
<tr>
<td color="#F36E21" width="100%" class="divider" style="border-bottom:1px solid #f36e21;border-left:none;display:block" height="1">&nbsp;</td>
</tr>
<tr>
<td>
<table cellspacing="0" cellpadding="0">
<tbody>
<tr valign='middle' height="25">
<td valign='middle' width="30">
<table cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td
style="vertical-align:bottom">
<span width="11" color="#F36E21" class="icons" style="display:block;background-color:#f36e21"><img src="https://www.hicaliber.com.au/wp-content/uploads/2019/09/phone-icon-2x.png" style="display:block" width="13"></span>
</td>
</tr>
</tbody>
</table>
</td>
<td style="padding:0px;color:#000000"> <span class="fs" style="" id="phone"> <a href="tel:1300%20664%20879" style="text-decoration:none;color:#000000;font-size:12px" target="_blank">1300 664 879</a>&nbsp;|&nbsp;<a href="tel:0403%20068%20555" color="#000000" style="text-decoration:none;color:#000000;font-size:12px"target="_blank">0403 068 555</a></span>
</td>
</tr>
<tr valign='middle' height="25">
<td valign='middle' width="30">
<table cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="vertical-align:bottom">
<span width="11" color="#F36E21" class="icons" style="display:block;background-color:#f36e21"> <img src="https://www.hicaliber.com.au/wp-content/uploads/2019/09/email-icon-2x.png" color="#F36E21" style="display:block" width="13"></span>
</td>
</tr>
</tbody>
</table>
</td>
<td style="padding:0px"><span class="fs" style=""><a href="mailto:your.email@hicaliber.com.au" color="#000000" style="text-decoration:none !important;color:#000000;font-size:12px" target="_blank" id="email">your.email@hicaliber.com.au</a></span> </td>
</tr>
<tr valign='middle' height="25">
<td valign='middle' width="30">
<table cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="vertical-align:bottom">
<span width="11" color="#F36E21" class="icons" style="display:block;background-color:#f36e21"> <img src="https://www.hicaliber.com.au/wp-content/uploads/2019/09/link-icon-2x.png" color="#F36E21" style="display:block" width="13"></span>
</td>
</tr>
</tbody>
</table>
</td>
<td style="padding:0px"><span class="fs" style="" id="websiteUrl"><a href="#" color="#000000" style="text-decoration:none !important;color:#000000;font-size:12px" target="_blank">
www.website.com
</a></span>
</td>
</tr>
<tr valign='middle' height="25">
<td valign='middle' width="30">
<table cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="vertical-align:bottom">
<span width="11" class="icons" style="display:block;background-color:#f36e21"> <img src="https://www.hicaliber.com.au/wp-content/uploads/2019/09/address-icon-2x.png" style="display:block" width="13"></span>
</td>
</tr>
</tbody>
</table>
</td>
<td><span style="font-size:12px;color:#000000"><span class="fs" style="" id="address">Gold Coast, Australia</span></span>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</table>
</td>
<td class="testingclas">&nbsp;</td>
</tr>
    <tr>
        <td colspan="3" hieght="15">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">
            <table cellspacing="0" cellpadding="0"><tr id="partnersLogos"><td></td></tr></table>
        </td>
    </tr>
    <tr>
        <td colspan="3" hieght="15">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3" hieght="15" id="banner">
            <img src="https://via.placeholder.com/700x170?text=Banner+Photo" width="700"/>
        </td>
    </tr>
</tbody>

</table>`

export default layout1