import layout1 from './layout1.js'
import layout2 from './layout2.js'

const layoutCollection = {
    layout1: layout1,
    layout2: layout2
}

export default layoutCollection