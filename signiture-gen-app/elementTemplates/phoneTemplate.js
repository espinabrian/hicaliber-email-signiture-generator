function phoneTemplate(val){
    const phoneList = val.split(',')
    const output = phoneList.map(phone => 
            (`
                <a href="tel:${phone}" style="text-decoration:none !important;color:#000000;font-size:12px" target="_blank">
                    ${phone}
                </a>
            `)
        ).join(' | ')
    return output
}

export default phoneTemplate