function socialTemplate( val , type) {

    const icons = {
        facebook : 'https://www.hicaliber.com.au/wp-content/uploads/2019/09/facebook-icon-2x.png',
        twitter : './signiture-gen-app/assets/images/twitter-icon-2x.png',
        linkedin : 'https://www.hicaliber.com.au/wp-content/uploads/2019/09/linkedin-icon-2x.png'
    }

    return (`&nbsp;
        <a 
            href="${val}" 
            style="display:inline-block;padding:0px;background-color:#f36e21" 
            target="_blank">
            <img src="${icons[type]}" alt="linkedin" color="#F36E21" style="max-width:135px;display:block" height="24">
        </a>
    `)
}

export default socialTemplate