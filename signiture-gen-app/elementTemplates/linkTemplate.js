function linkTemplate(val, type = false){

    const prefix = type === 'email' ?  'mailto:' : '//'

    return(`
        <a href="${prefix}${val}" color="#000000" style="text-decoration:none !important;color:#000000;font-size:12px" target="_blank">
            ${val}
        </a>
    `)
}

export default linkTemplate