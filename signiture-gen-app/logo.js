//import utility functions 
import { sanitize, clearElement } from './utility.js'

//import element templates
import socialTemplate from './elementTemplates/socialTemplate.js'
import logoTemplate from './elementTemplates/logoTemplate.js'


function logo(selector){
   //check if selector is undefined 
    if(!selector) return

    const elements = document.querySelectorAll(selector)

    elements.forEach(element => {
        element.addEventListener('keyup', event =>{
            const selector = event.target.dataset.targetNode
            const value = sanitize(event.target.value)
            const sigSelector = document.querySelector(`#${selector}`)

            if(!value) {
                clearElement(sigSelector) 
                return
            }

            switch(selector){

                case 'logourl':
                    sigSelector.innerHTML = logoTemplate(value)
                    break

                case 'linkedin':
                    sigSelector.innerHTML = socialTemplate(value, selector)
                    break

                case 'facebook':
                    sigSelector.innerHTML = socialTemplate(value, selector)
                    break
                
                case 'twitter':
                    sigSelector.innerHTML = socialTemplate(value, selector)
                    break

                default:
                    sigSelector.innerHTML = value
            }
        })
    })

}   
export default logo