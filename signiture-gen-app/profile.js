//import utility functions 
import { sanitize, clearElement } from './utility.js'

//import element templates
import photoTemplate from './elementTemplates/photoTemplate.js'
import phoneTemplate from './elementTemplates/phoneTemplate.js'
import bannerTemplate from './elementTemplates/bannerTemplate.js'
import linkTemplate from './elementTemplates/linkTemplate.js'

function profile(selector){
   //check if selector is undefined 
    if(!selector) return

    const elements = document.querySelectorAll(selector)

    elements.forEach(element => {
        element.addEventListener('keyup', event =>{
            const selector = event.target.dataset.targetNode
            const value = sanitize(event.target.value)
            const sigSelector = document.querySelector(`#${selector}`)

            if(!value) {
                clearElement(sigSelector) 
                return
            }

            switch(selector){

                case 'phone':
                    sigSelector.innerHTML = phoneTemplate(value)
                    break
                
                case 'photo':
                    sigSelector.innerHTML = photoTemplate(value)
                    break

                case 'banner':
                    sigSelector.innerHTML = bannerTemplate(value)
                    break
                
                case 'websiteUrl':
                    sigSelector.innerHTML = linkTemplate(value)
                    break

                case 'email':
                    sigSelector.innerHTML = linkTemplate(value, selector)
                    break

                default:
                    sigSelector.innerHTML = value
            }
        })
    })

}   
export default profile