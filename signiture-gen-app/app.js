//import panels functions
import design from './design.js'
import profile from './profile.js'
import logo from './logo.js'


//import utility functions 
import { copyToClipboard, sanitize} from './utility.js'

//expose copy to clipboard to window
window.copyToClipboard = copyToClipboard

//color State
const colorIsDefault = true

$(document).ready(function () {

    design()
    logo('.fs-input-logo')
    profile('.fs-input')

    function populateRepeater()   {
        $('.repeater .fs-input').on('keyup, change', function () {
            var d = $(this);
            var node = d.attr('data-target-node');
            var val = d.val();
            var partners = $('.repeater').repeaterVal();
            var partnerHtml = "";
                        
            $.each(  partners.partnerGroup , function(i , v){
                if( v.partnerLogoUrl && v.partnerName && v.partnerUrl ){
                    partnerHtml += "<td><a title='"+v.partnerName+"'  href='"+v.partnerUrl+"' rel='noopener noreferrer' target='_blank'>";
                    partnerHtml += "<img width='100' src='"+v.partnerLogoUrl+"' alt='"+v.partnerName+"'>";
                    partnerHtml += "</a></td><td width='10'></td>";
                } 
            });
            $('#partnersLogos').html(partnerHtml);
            
        }); 
    }

    populateRepeater();

    window.id = 0;

    $('.repeater').repeater({
        defaultValues: {
            'id': window.id,
        },
        show: function () {
            $(this).slideDown();
            $('#partner-id').val(window.id); 
            populateRepeater();
        },
        hide: function (deleteElement) {
            window.id--;
            $('#partner-id').val(window.id);
            $(this).slideUp(deleteElement);
            populateRepeater();
        },
        ready: function (setIndexes) {
            populateRepeater();
        }
    });
});



