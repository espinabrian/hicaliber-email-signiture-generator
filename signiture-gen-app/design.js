import layoutCollection from './layout-templates/index.js'
import { updateColor } from './utility.js'

function design(){
    
    //Layout
    const layoutElemnent = document.getElementById('email-signature')
    const layoutPicker = document.querySelectorAll('input[name="layoutPicker"]')
    if(layoutPicker.length > 0){
        layoutElemnent.innerHTML = layoutCollection.layout1 //set Default layout
        layoutPicker.forEach(element => {
            element.addEventListener('change', event=>{
                layoutElemnent.innerHTML = layoutCollection[event.target.value]
            })
        })
    }

    //Font Family
    const fontSelector = document.getElementById('font')
    fontSelector.addEventListener('change', event => {
        $('#email-signature table').attr( 'style' ,`font-family: ${event.target.value}`);
    })
    
    //Color Selector
    const colorPicker = document.getElementById('color')
    colorPicker.addEventListener('change', updateColor()) //update color in utility

}

export default design
