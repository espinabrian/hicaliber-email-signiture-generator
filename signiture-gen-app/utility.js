//Copt Signiture
export function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).html()).select();
    var str = $(element).html();

    function listener(e) {
        e.clipboardData.setData("text/html", str);
        e.clipboardData.setData("text/plain", str);
        e.preventDefault();
    }
    document.addEventListener("copy", listener);
    document.execCommand("copy");
    document.removeEventListener("copy", listener);

    $temp.remove();
    $("#success").removeClass('d-none');
    return false;
}

//Sanitize input
export function sanitize(s) {
    return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
}

//Clear Element
export function clearElement(element){
    element.innerHTML = ''
}

//Color
export function updateColor(colorState){
    return function(){
        //get current color 
        const currentColor = document.getElementById('color').value

        const images = document.querySelectorAll('#email-signature img')
        const divider = document.querySelector('.divider')

        //Control Panel
        document.querySelector('body').setAttribute('style', `--maincolor: ${currentColor}`)

        //Icons 
        images.forEach(element => {
            element.setAttribute('style', `background-color: ${currentColor};display:block`)
        })

        //dividers
        divider.setAttribute('style', `border-bottom:1px solid ${currentColor};border-left:none;display:block`)
    }
    
}